-- create database egrid;
-- psql -d egrid -f database_init.sql
-- drop table if exists hourly;
create table hourly (
    "index" int8 NOT NULL,
    "time" timestamp NOT NULL,
    "ORISPL_CODE" int8 ,
    "UNITID" character(8) ,
    "STATE" character(8) ,
    "FACILITY_NAME" character(40) ,
    "OP_TIME" float8,
    "GLOAD (MW)" float8 ,
    "SO2_MASS (lbs)" float8 ,
    "NOX_MASS (lbs)" float8 ,
    "CO2_MASS (tons)" float8 ,
    "HEAT_INPUT (mmBtu)" float8 ,
    "FAC_ID" int8 ,
    "UNIT_ID" int8 
);

create index "orispl_idx" ON hourly("ORISPL_CODE") ;
create index unitid_index on hourly("UNIT_ID");
create index plant_unit_index on hourly("ORISPL_CODE","UNIT_ID");
create index "time_idx" ON hourly("time" ASC);

