import numpy as np
import pandas as pd
import sys, os
from power_data.utils import tex_representation
from minpower.bidding import get_line_value, pairwise, _drop_dup_slopes, get_slopes
from utils import set_trace, debug_exceptions

debug_exceptions()

bpa_datadir = os.path.expanduser(
    '~/research/bpa-experiment/data/')
    
filename_project_data = bpa_datadir + 'BPA-project-data.xls'
filename_network_data = bpa_datadir + 'reservoir-network.csv'
filename_prices = bpa_datadir + 'midc-prices-2012.csv'
filename_export_max = bpa_datadir + 'BPA-intertie-capacity-2012.csv'

basedir = '../data-parsed/BPA/'

def curve_value(el, df, x='x', y='y'):
    vol_el_vals = df[[x, y]].dropna().values
    
    for A, B in pairwise(vol_el_vals):
        if A[0] <= el <= B[0]:
            return get_line_value(A, B, el)
    else: raise ValueError('elevation not found in curve domain')

 

def get_gen_data(xls):
    capacities = xls.parse('UnitCapacities',
        skiprows=[0, 1, 2], na_values='----')\
        .ix[:28]\
        .set_index('Unit')\
        .astype(float)

    # add power capacities
    gen_data = xls.parse('FacilityDataFormatted', skiprows=1)\
        .set_index('name')\
        .astype(float)
    gen_data['pmax'] = capacities.sum()

    # add network positions and lags
    network = pd.read_csv(filename_network_data,
        index_col=0)
    gen_data = gen_data.join(network)
    return gen_data


def get_curve_data(xls):
    # get production curve data
    # and conversion from flow to tailwater elevation
    pos_prod = {
        'BON': dict(skiprows=4, skip_footer=24, parse_cols='A:D'),
        'TDA': dict(skiprows=4, skip_footer=24, parse_cols='F:I'),
        'JDA': dict(skiprows=4, skip_footer=24, parse_cols='K:N'),
        'MCN': dict(skiprows=4, skip_footer=24, parse_cols='P:S'),
        'GCL': dict(skiprows=4, skip_footer=24, parse_cols='U:X'),
        'IHR': dict(skiprows=31, parse_cols='A:D'),
        'LMN': dict(skiprows=31, parse_cols='F:I'),
        'LGS': dict(skiprows=31, parse_cols='K:N'),
        'LWG': dict(skiprows=31, parse_cols='P:S'),
        'CHJ': dict(skiprows=31, parse_cols='U:X'),
        }

    curve_data = {}
    tw_data = {}
    for nm, kwds in pos_prod.iteritems():
        df = xls.parse('EnergyProduction', **kwds)\
            .drop(0)\

        df_flow = df[['Head', 'H/K']].dropna().astype(float)
        # df_flow['H/K'] = 1 / df['H/K']
        df_flow = df_flow.rename(columns={
            'Head': 'head',
            'H/K': 'outflow_coef'
            })[['head', 'outflow_coef']]

        # curve_data[nm] = pd.DataFrame(
        #     [[0,0]], index=[0], columns=df_flow.columns
        #     ).append(df_flow)
        curve_data[nm] = df_flow.reset_index(drop=True)
        
        df_tail = df[['Flow', 'TW']].rename(columns={
            'Flow': 'flow',
            'TW': 'elevation_tw'
            }).dropna().astype(float)
        tw_data[nm] = df_tail

    curve_data = pd.Panel(curve_data)
    tw_data = pd.Panel(tw_data)


    # get conversion from flow to elevation
    pos_res = {
        'BON': dict(skiprows=4, skip_footer=65, parse_cols='A:B'),
        'TDA': dict(skiprows=4, skip_footer=65, parse_cols='D:E'),
        'JDA': dict(skiprows=4, skip_footer=65, parse_cols='G:H'),
        'MCN': dict(skiprows=4, skip_footer=65, parse_cols='J:K'),
        'CHJ': dict(skiprows=4, skip_footer=65, parse_cols='M:N'),
        'GCL': dict(skiprows=4, parse_cols='P:Q'),
        'IHR': dict(skiprows=30, parse_cols='A:B'),
        'LMN': dict(skiprows=30, parse_cols='D:E'),
        'LGS': dict(skiprows=30, parse_cols='G:H'),
        'LWG': dict(skiprows=30, parse_cols='J:K'),
        }
    res_data = {}
    for nm, kwds in pos_res.iteritems():
        df = xls.parse('ReservoirContent', **kwds)\
            .drop(0)\
            [['Storage', 'Elevation']]\
            .dropna().astype(float)
        
        # convert kcfs * day to kcfs * hr
        # df.Storage *= 24.0
        # note: don't need to convert - units are stated in kcfs * day 
        #  but are actually already in kcfs * hr
        
        res_data[nm] = df.rename(columns={
            'Storage': 'volume',
            'Elevation': 'elevation_fb'
            })
    res_data = pd.Panel(res_data)

    return dict(
        flow_to_tailwater_elevation= tw_data,
        volume_to_forebay_elevation= res_data,
        head_to_production_coefficient= curve_data,
        )

def model_poly(df, deg=1, xnm='head', ynm='outflow_coef'):
    poly_coeffs = np.polyfit(df[xnm], df[ynm], deg=deg)
    
    # maximum model deviation from linear
    max_deviation = (
        df[ynm] - np.polyval(np.poly1d(poly_coeffs), df[xnm])
        ).apply(np.abs).max()
    return dict(
        poly_coeffs=poly_coeffs,
        max_deviation=max_deviation)

def reduce_curves(curve_name, coeffs, gen_data):
    new_coeffs = {}
    if curve_name == 'head_to_production_coefficient':
        max_linear_deviation = 10 # MW at max turbine outflow
        max_turbine_flow = lambda nm: gen_data.pmax[nm] / coeffs[nm].outflow_coef.max()
        lin_dev = pd.Series({nm: 
            max_turbine_flow(nm) * model_poly(coeffs[nm].dropna())['max_deviation']
            for nm in coeffs.items})

        for nm in coeffs.items:
            if lin_dev[nm] < max_linear_deviation:
                # take just the first and last point
                new_coeffs[nm] = coeffs[nm].dropna().iloc[[0,-1]]
            elif nm == 'GCL':
                #new_coeffs[nm] = _drop_dup_slopes(
                #    coeffs[nm], x='head', y='outflow_coef', tol=2e-2)
                new_coeffs[nm] = coeffs[nm].dropna().iloc[[0, 1, 12]]
            elif nm == 'CHJ':
                new_coeffs[nm] = _drop_dup_slopes(
                    coeffs[nm].iloc[[0, 2, 5]].append(coeffs[nm].iloc[6:]),
                    x='head', y='outflow_coef', tol=2e-2)
            else:
                set_trace()

            new_coeffs[nm] = new_coeffs[nm].reset_index(drop=True)
    elif curve_name == 'flow_to_tailwater_elevation':
        for nm, df in coeffs.iterkv():
            df = df.dropna().reset_index(drop=True)
            # these are all two piece lines
            if nm == 'GCL':   new_coeffs[nm] = df.ix[[0, 9, 13]]
            elif nm == 'IHR': new_coeffs[nm] = df.ix[[0, 7, 16]]
            elif nm == 'LMN': new_coeffs[nm] = df.ix[[0, 3, 16]]
            elif nm == 'LWG': new_coeffs[nm] = df.ix[[0, 4, 16]]
            elif nm == 'LGS': 
                # just a flat value
                new_coeffs[nm] = '{}'.format(df.elevation_tw.unique()[0])            
            else: 
                new_coeffs[nm] = df.copy()    # TEST - don't simplify
#            else: 
#                # a simple polynomial a+bx+cx^2
#                new_coeffs[nm] = tex_representation(model_poly(df, 
#                    deg=2, xnm='flow', ynm='elevation_tw',
#                    )['poly_coeffs'])
    elif curve_name == 'volume_to_forebay_elevation':
        for nm, df in coeffs.iterkv():
            df = df.dropna().reset_index(drop=True)
            # these are all two piece lines
            if nm == 'CHJ':
                # elevation bounded on 930-956ft
                new_coeffs[nm] = df.ix[[0, 1, 8]]
            elif nm == 'MCN': 
                new_coeffs[nm] = df.ix[[0, 1, 8]]
                
            elif nm in ['JDA', 'GCL']:
                new_coeffs[nm] = _drop_dup_slopes(df, tol=5e-4,
                    x='volume', y='elevation_fb')
            elif nm in ['LMN', 'BON', 'LGS', 'IHR', 'BON']:
                # HACK - tweak the numbers a bit to make curves concave 
                ve = df.dropna().copy()
                if nm == 'LMN':
                    ve.ix[1, 'volume'] += -1.0
                    ve.ix[2, 'volume'] += 0.5
                elif nm == 'LGS':
                    ve.ix[2, 'volume'] += 0.02
                    ve.ix[3, 'volume'] += 0.15 
                elif nm == 'IHR': 
                    ve.ix[1, 'volume'] += -0.8
                elif nm == 'BON':
                    ve = _drop_dup_slopes(ve, tol=5e-3,
                        x='volume', y='elevation_fb').reset_index(drop=True)
                    ve.ix[6, 'volume'] += -0.8
                    ve.ix[7, 'volume'] += 0.9


                if (get_slopes(ve).diff() > 0).any():
                    set_trace()
                new_coeffs[nm] = ve

#            elif nm == 'GCL':
#                # Coulee is way over specified (+80pts)
#                # instead use only a few points 
#                vols = np.linspace(df.volume.min(), df.volume.max(), 10)
#                new_coeffs[nm] = pd.DataFrame({
#                    'volume': vols, 
#                    'elevation_fb': pd.Series(vols).apply(
#                        lambda v: curve_value(v, df, 
#                            x='volume', y='elevation_fb'))
#                        })
            else: 
                new_coeffs[nm] = df.copy()    # TEST - don't simplify
#            else:
#                # a simple polynomial a+bx+cx^2
#                if nm in ['JDA', 'TDA']: deg = 1
#                else: deg = 2
#                
#                if nm == 'TDA': 
#                    # just one segment in elevation bounds
#                    df = df.ix[4:5]
#                
#                new_coeffs[nm] = tex_representation(model_poly(df, 
#                    deg=deg, xnm='volume', ynm='elevation_fb',
#                    )['poly_coeffs'], digits=15)
    return new_coeffs


def main():    
    xls = pd.ExcelFile(filename_project_data)

    gen_data = get_gen_data(xls)

    PWcurves = get_curve_data(xls)
    for cnm, panel in PWcurves.items():
        dirnm = '{}{}{}'.format(basedir, 'original_curves/', cnm)
        os.system('mkdir -p ' + dirnm)
        for hnm, df in panel.iteritems():
            df.to_csv(os.path.join(dirnm, '{}.csv'.format(hnm)), index=False)
        
    
    
    vol = PWcurves['volume_to_forebay_elevation'].minor_xs('volume')
    head = PWcurves['head_to_production_coefficient'].minor_xs('head')
    
    gen_data['volume_min'] = vol.min()
    gen_data['volume_max'] = vol.max()
    gen_data['head_min'] = head.min()
    gen_data['head_max'] = head.max()
    
    gen_data = gen_data.join(pd.read_csv(
        basedir + 'historical-flows/outflow-limits.csv', index_col=0))    
    
    
    PWcurves_reduced = {nm: 
        reduce_curves(nm, PWcurves[nm], gen_data) 
        for nm in PWcurves.keys()}


    for curve_name, data in PWcurves_reduced.iteritems():
        dirnm = os.path.join(basedir, curve_name)
        os.system('mkdir -p {}'.format(dirnm))
        gen_data[curve_name] = ''
        for nm in gen_data.index:
            fnm = os.path.join(curve_name, '{}.csv'.format(nm))
            if type(data[nm]) == pd.DataFrame:            
                gen_data.ix[nm, curve_name] = fnm
                data[nm].dropna().to_csv(os.path.join(basedir, fnm), index=False)
            else:
                gen_data.ix[nm, curve_name] = data[nm]
                # write non-reduced pwl curves to file for consistency 
                PWcurves[curve_name][nm].dropna().to_csv(os.path.join(basedir, fnm), index=False)
    
    gen_data['inflow_schedule'] = \
        ['inflow_{}'.format(nm) for nm in gen_data.index]
    # inflow schedules are parsed in scenarios_data.py
    
    
    # write hydro file        
    gen_data.to_csv(basedir + 'hydro.csv')
    # write hydro file w targets
    # nms = ['GCL', 'CHJ', 'MCN', 'JDA', 'TDA']
    nms = gen_data.index

    gen_data['elevation_target_max_schedule'] = pd.Series(
        {sp: 'elevation_target_max_{}'.format(sp) for sp in nms})
    gen_data['elevation_target_min_schedule'] = pd.Series(
        {sp: 'elevation_target_min_{}'.format(sp) for sp in nms}) 
    gen_data.to_csv(basedir + 'hydro-w-targets.csv')
if __name__ == '__main__': main()
