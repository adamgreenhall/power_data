to get a full set of minpower parameters from EPA data
run:

    ipython EPA_data.ipynb
    python hourly_attributes.py
    ipython post-process-ercot-chp.ipynb
    ipython post-process-egrid-data.ipynb
    
