'''
Fetch the raw eGrid data from the EPA.
This includes two datasets - metadata (annual) and hourly data.
'''

import config
from utils import download, set_trace
import pandas as pd
from hourly_to_postgres import add_files_to_database, get_unit_metadata
from database import DatabaseConnection    
from datetime import datetime
import os

def fetch_raw_annual_data():
    """
    Go to the egrid website and download the zipfiles. Unzip.
    These contain plant,generator,boiler level information for fuel type and energy
    produced for the year.
    """
    zipfilenames = {
        2009: 'http://www.epa.gov/cleanenergy/documents/egridzips/eGRID2012_Version1-0.zip',
        # 2007:'http://www.epa.gov/cleanenergy/documents/egridzips/eGRID2010_Version1-1_xls_only.zip',
        # 2005:'http://www.epa.gov/cleanenergy/documents/egridzips/eGRID2007_Version1-1_xls_only.zip',
        # 2004:'http://www.epa.gov/cleanenergy/documents/egridzips/eGRID2006_Version2-1_xls_only.zip'
    }

    for year, web_filename in zipfilenames.iteritems():
        download(web_filename, config.dir_raw_annual)
        # homefilename=raw_annual_filename(kind='all',ext='.zip',year=year)


def fetch_raw_hourly_data(states=config.states,
    years=config.years, daterange=None):
    
    """
    Go to the EPA website and download the files for hourly emissions.
    Files are organized by year, month, state.
    """
    dir_web = 'ftp://ftp.epa.gov/dmdnload/emissions/hourly/monthly/'
    
    if daterange is None:
        daterange = pd.date_range(
            '{y}-01-01'.format(y=min(years)),
            '{y}-12-01'.format(y=max(years)), freq='MS')

    downloaded_files = []
    for state in states:
        for date in daterange:
            if date.year == config.this_year and date.month >= config.this_month:
                continue  # the future

            web_filename = dir_web + '{y}/{y}{s}{m:02d}.zip'.format(
                y=date.year, s=state.lower(), m=date.month)
            local_filename = '{y}{s}{m:02d}.csv'.format(
                y=date.year, s=state.lower(), m=date.month)
                
            if not os.path.exists(
                os.path.join(config.dir_raw_hourly, local_filename)):
                try:
                    download(web_filename, config.dir_raw_hourly)
                    downloaded_files.append(local_filename)
                    
                    print('downloaded {}'.format(local_filename))
                except IOError:
                    print('failed on {}'.format(web_filename))
                    continue
            else:
                downloaded_files.append(local_filename)

    return downloaded_files

def update_hourly_database():
    
    db = DatabaseConnection('egrid')
    Tmax = pd.Timestamp(db.max_query('time', 'hourly'))
    states = db.query('select distinct ("STATE") from hourly'
        )['STATE'].str.strip().values.tolist()

    # monthly start frequency
    months = pd.date_range(
        Tmax + pd.DateOffset(hour=0), 
        pd.Timestamp(datetime.now()), freq='MS')
    
    print('fetching data for:')
    print(states)
    print(months)
    
    new_files = fetch_raw_hourly_data(states=states, daterange=months)
    
    print('Fetched {} new files. Adding them to the database.'.format(
        len(new_files)))
    
    if new_files:
        add_files_to_database(new_files)
        get_unit_metadata()


def backfill_old_years():
    states = ['TX']

    # monthly start frequency
    months = pd.date_range('2003-01-01', '2008-12-31',freq='MS')
    
    print('fetching data for:')
    print(states)
    print(months)
    
    new_files = fetch_raw_hourly_data(states=states, daterange=months)
    
    print('Fetched {} new files. Adding them to the database.'.format(
        len(new_files)))
    
    if new_files:
        add_files_to_database(map(lambda f: os.path.join(config.dir_raw_hourly, f), new_files))
        get_unit_metadata()

if __name__ == '__main__':
    # update_hourly_database()
    get_unit_metadata()
    
    
    #fetch_raw_hourly_data(
    #    states=['WA', 'OR', 'ID', 'MT'], years=[2009, 2010, 2011, 2012])
