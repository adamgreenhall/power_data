"""
Find hourly attributes for each unit:
* power min/max
* ramp rate min/max
* min up/down time
* heat rate curve
"""

import logging
import config
from utils import (get_limits, tex_representation, runs,
                   fit_polynomial, load_yaml_file, set_trace)
from database import DatabaseConnection
import pandas as pd
import numpy as np
from parse_hourly import get_hourly_data
from parse_annual import parse_annual, get_plant_attributes


def get_attributes(unit):
    attributes = dict()

    # operating for a full hour and not changing status
    full_hour_ops = unit[(unit.op_time == 1) &
                         (unit.op_time.diff() == 0) &
                         (unit.op_time.shift(-1).diff() == 0)]

    # Find the power limits
    attributes['power_min'], attributes['power_max'] = \
        get_limits(full_hour_ops.power)

    # Find the ramp limits
    ramp = full_hour_ops.power.diff()
    attributes['ramp_min'], attributes['ramp_max'] = \
        get_limits(ramp, freq_limit=0.01)

    # Find the up/down time limits
    # this depends on some sneaky itertools magic
    # to implement run length encoding
    operating = unit.op_time == 1
    not_operating = unit.op_time == 0

    operating_runs = runs(operating.values.tolist(), more_than_count=1)
    not_operating_runs = runs(not_operating.values.tolist(), more_than_count=1)

    if len(operating_runs) > 0:
        attributes['min_up_time'] = min(operating_runs)
    if len(not_operating_runs) > 0:
        attributes['min_down_time'] = min(not_operating_runs)

    heat_curve = get_heatrate(full_hour_ops,
                              pmin=attributes['power_min'],
                              pmax=attributes['power_max'])

    if heat_curve:
        attributes['heat_curve'] = heat_curve

    # unit = unit.reset_index()
    # attributes['startup_ramp_limit'] = \
    #     unit.ix[unit[(status==1) & (status.diff()>0)].index, col_energy].max()
    #
    # attributes['shutdown_ramp_limit'] = -1 * \
    # unit.ix[unit[(status == 0) & (status.diff()<0)].index + -1,
    # col_energy].max()

    # TODO - these limits can be contradictory with rampmax and Pmin

    return attributes


def get_heatrate(full_hour_ops, pmin, pmax, Nbins=20, round_digits=15):
    within_limits = full_hour_ops[
        (pmin <= full_hour_ops.power) & (full_hour_ops.power <= pmax)]

    # there aren't enough values to fit a curve
    if len(within_limits) < 10:
        return

    bins = pd.cut(within_limits.power, Nbins)
    bin_means = within_limits[['power', 'heat']].groupby(bins).mean().dropna()

    # not enough values to fit a curve
    if len(bin_means) < 3:
        return

    # get fits from linear to cubic
    fits = dict()
    for deg in [1, 2, 3]:
        coefs, resid = fit_polynomial(bin_means.values, deg)
        fits[deg] = dict(coefs=coefs, resid=resid)

    # valid fits are monotonically increasing
    valid_fits = {deg: fit for deg, fit in fits.items()
                  if monotonically_increasing(fit['coefs'])}

    if len(valid_fits.keys()) == 0:
        # check if all the heat rates are similar
        heat_val = bin_means.heat.ix[0]

        # if all bin means for heat are the same
        if all(bin_means.heat == heat_val):
            # the best fit is a fixed point
            best_fit = dict(coefs=[heat_val])
        else:
            logging.warning('no good fit found. fallback to linear')
            best_fit = fits[1]
    else:
        # the best fit is the lowest residual
        best_fit = sorted(valid_fits.items(),
                          key=lambda fit: fit[1]['resid'])[0][1]

    return tex_representation(best_fit['coefs'], digits=round_digits)


def monotonically_increasing(coefs):
    if len(coefs) <= 1:
        return True  # if linear
    elif all([c >= 0 for c in coefs[:-1]]): return True  # if all terms (but the intercept) are positive
    else:
        return False


def unit_key(plant_id, gen_id='all'):
    return '{}_{}'.format(plant_id, gen_id)

attributes_list = [
    'name',
    'plant_id',
    'generator_id',
    'power_min',
    'power_max',
    'heat_curve',
    'ramp_min',
    'ramp_max',
    'startup_ramp_limit',
    'shutdown_ramp_limit',

    'min_up_time',
    'min_down_time',

    'fuel',
    'prime_mover',

    'nominal_plant_heat_rate',
    'chp',
    'nameplate',

]

def main():

    mapping = load_yaml_file('{d}annual-hourly-mapping-{r}.yaml'.format(
        d=config.dir_parsed, r=config.region))

    generator_index = []
    for plant_id, generator_unit_mapping in mapping.items():
        if len(generator_unit_mapping.keys()) == 0:
            generator_index.append(unit_key(plant_id, 'all'))
        else:
            for gen_id in generator_unit_mapping.keys():
                generator_index.append(unit_key(plant_id, gen_id))

    gen_attributes = pd.DataFrame(
        columns=attributes_list,
        index=sorted(generator_index))

    def set_attributes(index, dict):
        for col, val in dict.items():
            gen_attributes[col][index] = val

    boilers, generators, plants = parse_annual()

    db = DatabaseConnection('egrid')
    for plant_id, generator_unit_mapping in sorted(mapping.items()):
        print plant_id
        if len(generator_unit_mapping.keys()) == 0:
            set_attributes(
                unit_key(plant_id), 
                get_plant_attributes(plant_id, plants, generators))
        else:
            for gen_id, unit_id in generator_unit_mapping.items():
                if 'combined' in str(gen_id):
                    # a one-to-many mapping
                    unit_data = get_hourly_data(plant_id, unit_id, db)
                    
                    # group into a combined unit
                    unit_data = unit_data.reset_index().groupby('time').agg({
                        'power': np.sum,
                        'heat': np.sum,
                        'op_time': np.max}).asfreq('H')
                else:
                    # a one-to-one gen-unit mapping
                    unit_data = get_hourly_data(
                        plant_id, [unit_id], db).asfreq('H')
            
                attributes = get_attributes(unit_data)
                
                plant_gen_id = None if 'combined' in str(gen_id) else gen_id
                # generator attributes
                attributes.update(get_plant_attributes(plant_id, 
                    plants, generators, plant_gen_id))
                set_attributes(unit_key(plant_id, gen_id), attributes)

    gen_attributes.to_csv(
        config.dir_out + 'generators-{}.csv'.format(config.region))

    db.con.close()


if __name__ == "__main__":
    config.region = "BPA"
    main()
    
    # to post process into a form suitible for minpower
    # run post-process-egrid-data.ipynb
