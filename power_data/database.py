import keyring
import getpass
import psycopg2 as database_driver_pg
from pandas.io.sql import read_frame as dataframe_from_sql

try:
    import MySQLdb as database_driver_mysql
except ImportError:
    database_driver_mysql = None


def query(sql, con):
    ''' execute a query and fetch the results as a list '''
    cur = _execute(sql, con)
    rows = _safe_fetch(cur)
    return rows


def sql_in(list, quotes=False):
    '''
    consructs the list for an "IN" query:
    >>> sql_in([5,6,7,99])
    (5,6,7,99)
    >>> sql_in(['Hansen','Pettersen'], quotes=True)
    ('Hansen','Pettersen')
    '''
    if quotes:
        return "('{}')".format("','".join(list))
    else:
        return '({})'.format(','.join([str(item) for item in list]))


def _execute(sql, con, cur=None, params=None, commit=False):
    """
    Execute the given SQL query using the provided connection object.

    Parameters
    ----------
    sql: string
        Query to be executed

    Returns
    -------
    Cursor object
    """
    try:
        if cur is None:
            cur = con.cursor()

        if params is None:
            cur.execute(sql)
        else:
            cur.execute(sql, params)
        
        if commit:
            con.commit()
        return cur
    except Exception:
        try:
            con.rollback()
        except Exception:
            pass
        print 'Error on sql %s' % sql
        raise


def _safe_fetch(cur):
    try:
        result = cur.fetchall()
        if not isinstance(result, list):
            result = list(result)
        return result
    except Exception, e:  # pragma: no cover
        excName = e.__class__.__name__
        if excName == 'OperationalError':
            return []


class DatabaseConnection(object):
    def __init__(self, db_name,
        username='adam', host='localhost', db_type='pg', reset_pass=False):
        self.type = db_type
        self.params = dict(database=db_name, host=host, user=username)
        
        if self.type == 'pg':
            self.database_driver = database_driver_pg
        elif self.type == 'mysql':
            self.database_driver = database_driver_mysql

        if self.database_driver is None:
            raise ImportError('The database driver package for {} wasnt found.'.format(self.type))

        self.con = None
        self.authenticate(reset_pass)
        
    def authenticate(self, reset_pass=False):
        app_name = self.params['database'] + '_access_app'
        store_pass = False
        pwd = None      
        
        if not reset_pass:
            try:
                pwd = keyring.get_password(app_name, self.params['user'])
            except:
                pwd = None

        if pwd is None or pwd == '':
            pwd = getpass.getpass(prompt=
                'Password for {} on {} database:'.format(
                    self.params['user'], self.params['database']))
            store_pass = True

        self._con_params = self.params.copy()
        self._con_params.update(
            dict(db=self.params['database'], passwd=pwd, password=pwd))
        if self.type == 'pg':
            self._con_params.pop('passwd', '')
            self._con_params.pop('db', '')
        elif self.type == 'mysql':
            self._con_params.pop('password', '')
            self._con_params.pop('database', '')

        if store_pass:
            keyring.set_password(app_name, self.params['user'], pwd)
        

    def connect(self):
        self.con = self.database_driver.connect(**self._con_params)
        return self.con

    def query(self, q):
        if self.con is None: self.connect()
        return dataframe_from_sql(q, self.con)

    def max_query(self, column, table, condition=""):
        q = 'select max("{col}") from {t} {cond}'.format(
            col=column, t=table, cond=condition)
        return self.query(q).ix[0, 'max']
        

    def __enter__(self):
        return self.connect()

    def __exit__(self, type, value, traceback):
        self.con.close()
        self.con = None
