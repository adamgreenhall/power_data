'''
get data from CAISO
currently set up to get the observed demand for 2012
but with some work, should be able to pull data for any of the CAISO datasets 


WARNING: queries return a zipped csv file that may not be in date order. 
WARNING: only one month can be pulled at a time or you will get an xml error file instead of a csv.

observed data
http://oasis.caiso.com/mrtu-oasis/SingleZip?resultformat=6&queryname=SLD_FCST&market_run_id=ACTUAL&startdate=20120101&enddate=20120131

day ahead forecast data
http://oasis.caiso.com/mrtu-oasis/SingleZip?resultformat=6&queryname=SLD_FCST&market_run_id=DAM&startdate=20120101&enddate=20120131
'''

import pandas as pd
import urllib, zipfile
from zipfile import ZipFile
from pandas.util.py3compat import StringIO
from ipdb import set_trace

def get_url(start_date, end_date, kind='demand_observed'):
    queries = {'demand_observed': 'queryname=SLD_FCST&market_run_id=ACTUAL'}
    return urllib.urlopen('http://oasis.caiso.com/mrtu-oasis/SingleZip?resultformat=6&{query}&startdate={s}&enddate={e}'.format(
        query=queries[kind],
        s=start_date.strftime('%Y%m%d'),
        e=end_date.strftime('%Y%m%d')))


def parse_load_data(csv):
    df = pd.read_csv(csv, parse_dates=['OPR_DT'])
    df = df[df.TAC_AREA_NAME == 'CA ISO-TAC'].sort('OPR_DT').reset_index(drop=True)
    cols = ['HE{:02d}'.format(h) for h in range(1,26)]
    out = pd.Series(
        pd.Series(df[cols].values.flatten()).dropna().values,
        index=pd.date_range(
            df['OPR_DT'].iloc[0],
            df['OPR_DT'].iloc[-1] + pd.DateOffset(hour=23),
            freq='H', tz='US/Pacific'))
    return out

demand_data = pd.Series()

year = '2010'
start_date = '{}-01-01'.format(year)
end_date = '{}-12-31'.format(year)

for month_end_date in pd.date_range(start_date, end_date, freq='M'):
    start_date = month_end_date + pd.DateOffset(day=1)
    url = get_url(start_date, month_end_date, 'demand_observed')
    zipfile = ZipFile(StringIO(url.read()))
    demand_data = demand_data.append(
        parse_load_data(zipfile.open(zipfile.filelist[0].filename)))
    
demand_data.name = 'power'
demand_data.index.name = 'time'

demand_data.to_csv('data-parsed/CAISO/CAISO-demand-{}.csv'.format(year), header=True)

print demand_data.describe()
