import config
import pandas as pd

col_cap = 'NAMEPCAP'
col_gen_id = 'SEQGEN09'

important_columns_generator = ['GENID', col_gen_id, col_cap, 'NUMBLR',
                               'PRMVR', 'FUELG1']
important_columns_plant = ['PNAME', 'NERC', 'NAMEPCAP', 'NUMBLR', 'NUMGEN']


def parse_annual(rm_backup_gen=True):
    yr = '09'
    xls = pd.ExcelFile(config.dir_raw_annual + 'eGRID2012V1_0_year09_DATA.xls')
    boilers = xls.parse('BLR' + yr, header=5)
    generators = xls.parse('GEN' + yr, header=5)
    # note: don't set the SEQGEN as an index - it has missing values
    plants = xls.parse('PLNT' + yr, index_col=0, header=4)

    boilers['ORISPL'] = boilers.ORISPL.astype(int)
    generators['ORISPL'] = generators.ORISPL.astype(int)
    generators['SEQGEN09'] = generators.SEQGEN09.astype(int)
    plants['ORISPL'] = plants.ORISPL.astype(int)


    plants = plants.set_index('ORISPL')
    generators = generators.set_index('SEQGEN09')
    
    if rm_backup_gen:
        # ignore the fuel oil IC backup generators
        generators = generators[
            ((generators['FUELG1'] == 'DFO') & 
                (generators['PRMVR'] == 'IC')) == False]

        # ignore mothballed or unfinished generators
        generators = generators[generators.GENSTAT == "OP"]
        
    return boilers, generators, plants

def get_plant_attributes(plant_id, plants, generators, gen_id=None):
    no_gen = (gen_id is None)

    plant = plants.ix[plant_id]
    if not no_gen:
        # get a series of generator properties
        gen = generators.ix[gen_id]

    return dict(
        name=plant['PNAME'] if no_gen else '{p} {u}'.format(
            p=gen['PNAME'], u=gen['GENID']),
        plant_id=plant_id,
        generator_id='all' if no_gen else gen_id,
        fuel=get_fuel_type(
            plant['PLPRMFL']) if no_gen else get_fuel_type(gen['FUELG1']),
        prime_mover='' if no_gen else gen['PRMVR'],
        chp=plant['CHPFLAG'] == 1,
        nominal_plant_heat_rate=plant['PLHTRT'],
        nameplate=plant['NAMEPCAP'] if no_gen else gen['NAMEPCAP'],
    )

def get_fuel_type(fuel):
    return config.fuel_types.get(fuel, fuel)

def filter_to_id(df, id_num):
    if df.index.name == 'ORISPL':
        return df[df.index == id_num]
    else:
        col = 'ORISPL_CODE' if 'ORISPL_CODE' in df.columns else 'ORISPL'
        return df[df[col] == id_num]

# generator filters


def filter_operating(df):
    return df[df['GENSTAT'] == 'OP']


def filter_has_boilers(df):
    return df[df['NUMBLR'] > 0]

# plant filters
def filter_to_region(plants, region=config.region):
    region_plants = plants.copy()
    for key, values in config.coverage_iso[region].iteritems():
        region_plants = region_plants[region_plants[key].isin(values)]
    return region_plants
