import numpy as np
import pandas as pd
from ipdb import set_trace
import sys, os, glob

from minpower.bidding import get_line_value, pairwise

# extend DataFrame to have interpolate method
def interpolate(df, **kwds):
    return pd.DataFrame({
        nm: series.interpolate(**kwds) 
        for nm, series in df.iterkv()})[df.columns]
pd.DataFrame.interpolate = interpolate

def debug_exceptions():
    from IPython.core import ultratb
    sys.excepthook = ultratb.FormattedTB(mode='Verbose',
        color_scheme='Linux', call_pdb=1)

def ts_from_csv(filename, tz='US/Pacific'):
    return pd.read_csv(filename, index_col=0, parse_dates=True, squeeze=True)\
            .tz_localize('UTC').tz_convert(tz)
            
def get_volume_from_elevation(df, el):
    vol_el_vals = df[['elevation_fb', 'volume']]\
        .dropna().values
        
    for A, B in pairwise(vol_el_vals):
        if A[0] <= el <= B[0]:
            return get_line_value(A, B, el)
    else: raise ValueError('elevation not found in curve domain')


def read_dir_to_panel(dirnm, **kwds):
    return pd.Panel({
        os.path.split(fnm)[1].strip('.csv'): pd.read_csv(fnm, **kwds)
        for fnm in glob.glob(os.path.join(dirnm, '*.csv'))
        })

