'''
get limit data for BPA transmission limits
currently set up to get the hourly 2012
pull down one month at a time

Summary at:
http://transmission.bpa.gov/Business/Operations/intertie/

Monthly excel files like:
http://transmission.bpa.gov/Business/Operations/intertie/interties/monthly/ACDC/2012_ACDC_12.xls
'''

import pandas as pd
import urllib, zipfile
from zipfile import ZipFile
from pandas.util.py3compat import StringIO
from ipdb import set_trace

def get_url(timestamp, kind='ACDC'):
    return urllib.urlopen('http://transmission.bpa.gov/Business/Operations/intertie/interties/monthly/ACDC/{year}_{kind}_{month:02d}.xls'.format(
        kind=kind,
        year=timestamp.year,
        month=timestamp.month))


def parse_transmission_capacity(xls,
    sheet_name='Sheet1', col_nm_start='AC+DC CAPACITY: N to S'):
    
    out = xls.parse(sheet_name, skiprows=10).set_index('Date/Time')
    out = out[filter(lambda c: c.strip().startswith(col_nm_start), out.columns)[0]]
    out.name = 'power'
    out.index.name = 'time'
    return out

data = pd.Series()

year = '2012'
start_date = '{}-01-01'.format(year)
end_date = '{}-12-31'.format(year)

month_ends = pd.date_range(start_date, end_date, freq='M')
for month_end_date in month_ends:
    print month_end_date
    url = get_url(month_end_date, 'ACDC')
    xls = pd.ExcelFile(StringIO(url.read()))
    data = data.append(
        parse_transmission_capacity(xls))
    
data.name = 'power'
data.index = pd.date_range(start_date + ' 00:00:00', end_date + ' 23:00:00',
    freq='H', tz='US/Pacific')
data.index.name = 'time'
data.interpolate().to_csv('../data-parsed/BPA/BPA-intertie-capacity-{}.csv'.format(year), header=True)

print(data.describe())
