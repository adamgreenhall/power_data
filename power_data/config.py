'''
The configuration for the generation data creation.
'''
import os
from datetime import datetime 
this_year = datetime.now().year
this_month = datetime.now().month

years=range(2009,this_year+1) #2006,2007,2008
region = 'ERCOT'

coverage_iso = dict(
    ERCOT={ 
        'PSTATABB': ['TX','OK'], 
        'NERC': ['ERCOT','TRE'], #in 2010 eGrid this switches to 'TRE'
        'SUBRGN': ['ERCOT All', 'ERCT'],
        },
    BPA={
        # state name
        'PSTATABB': ['WA','OR','ID'], 
        'NERC': ['WECC'],
        # NERC sub-region
        'SUBRGN': ['WECC Northwest', 'NWPP'],
        # power control area
        'PCANAME': ['Bonneville Power Administration']
        },
    )
states  = coverage_iso[region]['PSTATABB']


dir_out='data-created/'
dir_raw='data-raw/'
dir_raw_annual=dir_raw+'egrid/generation-annual/'
dir_raw_hourly=dir_raw+'egrid/generation-hourly/'
dir_parsed='data-parsed/'

logging_level = 'DEBUG'
default_number_bins = 20


fields_hourly_data=dict(
    name='FACILITY_NAME',
    epa_index='ORISPL_CODE',
    epa_ID_plant='FAC_ID',
    epa_ID_unit ='UNITID',
    state='STATE',
    )
fields_hourly_data_timed = dict(
    #time dependent information
    date='OP_DATE',
    hour='OP_HOUR',
    frachourON='OP_TIME',
    power='GLOAD (MW)',
    heat='HEAT_INPUT (mmBtu)',
    )


fields_annual_plant_data=dict(
    state='PSTATABB',
    name='PNAME',
    id='ORISPL',
    region='NERC',
    num_boilers='NUMBLR',
    num_generators='NUMGEN',
    fuel_primary='PLPRMFL',
    kind_fuel_primary='PLFUELCT',
    capacity_factor='CAPFAC',
    capacity='NAMEPCAP',
    CHP='CHPFLAG',
    energy_annual='PLNGENAN', #annual MWh 
    heat_rate_nominal='PLHTRT', #BTU/kWh --converts to MMBtu/MWh by div. by a factor of 1000 
    )

fields_annual_generator_data=dict(
    state='PSTATABB',
    plant_name='PNAME',
    plant_number='ORISPL',
    id='GENID',
    num_boilers='NUMBLR',
    status = 'GENSTAT',
    kind_prime_mover = 'PRMVR',
    fuel_primary = 'FUELG1',
    capacity='NAMEPCAP',
    capacity_factor='CFACT',
    energy_annual='GENNTAN', #annual MWh  
    )

fields_annual_unit_data=dict(
    state='PSTATABB',
    plant_name='PNAME',
    plant_number='ORISPL',
    id='BLRID',
    num_generators='NUMGEN',
    fuel_primary='FUELB1',
    operating_hours = 'HRSOP', #in 2010 this switches to 'HRSOP' in 2004 it was 'LOADHRS'
    heat_annual='HTIEAN', #MMBtu/yr  
    )

    
fuel_types=dict(
    ANT='coal',BIT='coal',LIG='coal',SUB='coal',WC='coal',RC='coal',
    DFO='oil',JF='oil',KER='oil',PC='oil',RFO='oil',WO='oil',
    BGF='natural gas',NG='natural gas',OG='natural gas',PG='natural gas',SG='natural gas',SGC='natural gas',
    NUC='nuclear',
    WAT='hydro',
    WND='wind'
    )

primemover_types=dict(
    bt='geothermal',
    ca='combined-cycle',
    ce='compressed air energy storage',
    cp='concentrated solar power',
    cs='combined-cycle single-shaft combustion turbine',
    ct='combined-cycle combustion turbine',
    fc='fuel cell',
    gt='combustion (gas) turbine',
    ha='hydro',
    hb='hydro',
    hk='hydro',
    hy='hydro',
    ps='hydro w pump storage',
    ic='internal combustion engine',
    ot='other',
    pv='photovoltaic',
    st='steam turbine',
    wt='wind turbine onshore',
    ws='wind turbine offshore',
    )    
def raw_month_filename(month,year,state='tx',ext='.csv'):
    return dir_raw_hourly+'{y}{s}{m:02d}'.format(y=year,s=state.lower(),m=month)+ext
def raw_annual_filename(kind='plants',ext='.csv',year=2007):
    return dir_raw_annual+'egrid-{y}-{k}'.format(y=year,k=kind)+ext


all_states = [
    { 'name': 'ALABAMA', 'abbreviation': 'AL'},
    { 'name': 'ALASKA', 'abbreviation': 'AK'},
    { 'name': 'AMERICAN SAMOA', 'abbreviation': 'AS'},
    { 'name': 'ARIZONA', 'abbreviation': 'AZ'},
    { 'name': 'ARKANSAS', 'abbreviation': 'AR'},
    { 'name': 'CALIFORNIA', 'abbreviation': 'CA'},
    { 'name': 'COLORADO', 'abbreviation': 'CO'},
    { 'name': 'CONNECTICUT', 'abbreviation': 'CT'},
    { 'name': 'DELAWARE', 'abbreviation': 'DE'},
    { 'name': 'DISTRICT OF COLUMBIA', 'abbreviation': 'DC'},
    { 'name': 'FLORIDA', 'abbreviation': 'FL'},
    { 'name': 'GEORGIA', 'abbreviation': 'GA'},
    { 'name': 'HAWAII', 'abbreviation': 'HI'},
    { 'name': 'IDAHO', 'abbreviation': 'ID'},
    { 'name': 'ILLINOIS', 'abbreviation': 'IL'},
    { 'name': 'INDIANA', 'abbreviation': 'IN'},
    { 'name': 'IOWA', 'abbreviation': 'IA'},
    { 'name': 'KANSAS', 'abbreviation': 'KS'},
    { 'name': 'KENTUCKY', 'abbreviation': 'KY'},
    { 'name': 'LOUISIANA', 'abbreviation': 'LA'},
    { 'name': 'MAINE', 'abbreviation': 'ME'},
    { 'name': 'MARYLAND', 'abbreviation': 'MD'},
    { 'name': 'MASSACHUSETTS', 'abbreviation': 'MA'},
    { 'name': 'MICHIGAN', 'abbreviation': 'MI'},
    { 'name': 'MINNESOTA', 'abbreviation': 'MN'},
    { 'name': 'MISSISSIPPI', 'abbreviation': 'MS'},
    { 'name': 'MISSOURI', 'abbreviation': 'MO'},
    { 'name': 'MONTANA', 'abbreviation': 'MT'},
    { 'name': 'NEBRASKA', 'abbreviation': 'NE'},
    { 'name': 'NEVADA', 'abbreviation': 'NV'},
    { 'name': 'NEW HAMPSHIRE', 'abbreviation': 'NH'},
    { 'name': 'NEW JERSEY', 'abbreviation': 'NJ'},
    { 'name': 'NEW MEXICO', 'abbreviation': 'NM'},
    { 'name': 'NEW YORK', 'abbreviation': 'NY'},
    { 'name': 'NORTH CAROLINA', 'abbreviation': 'NC'},
    { 'name': 'NORTH DAKOTA', 'abbreviation': 'ND'},
    { 'name': 'OHIO', 'abbreviation': 'OH'},
    { 'name': 'OKLAHOMA', 'abbreviation': 'OK'},
    { 'name': 'OREGON', 'abbreviation': 'OR'},
    { 'name': 'PALAU', 'abbreviation': 'PW'},
    { 'name': 'PENNSYLVANIA', 'abbreviation': 'PA'},
    { 'name': 'PUERTO RICO', 'abbreviation': 'PR'},
    { 'name': 'RHODE ISLAND', 'abbreviation': 'RI'},
    { 'name': 'SOUTH CAROLINA', 'abbreviation': 'SC'},
    { 'name': 'SOUTH DAKOTA', 'abbreviation': 'SD'},
    { 'name': 'TENNESSEE', 'abbreviation': 'TN'},
    { 'name': 'TEXAS', 'abbreviation': 'TX'},
    { 'name': 'UTAH', 'abbreviation': 'UT'},
    { 'name': 'VERMONT', 'abbreviation': 'VT'},
    { 'name': 'VIRGINIA', 'abbreviation': 'VA'},
    { 'name': 'WASHINGTON', 'abbreviation': 'WA'},
    { 'name': 'WEST VIRGINIA', 'abbreviation': 'WV'},
    { 'name': 'WISCONSIN', 'abbreviation': 'WI'},
    { 'name': 'WYOMING', 'abbreviation': 'WY' }
]
