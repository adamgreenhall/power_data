import pandas.io.date_converters as conv
from database import sql_in, dataframe_from_sql
from pandas import read_csv
import config

col_energy = 'GLOAD (MW)'
col_heat = 'HEAT_INPUT (mmBtu)'
col_frachr = 'OP_TIME'

important_columns = [
    'time',
    'ORISPL_CODE',
    'UNITID',
    'STATE',
    'FACILITY_NAME',
    col_frachr,
    col_energy,
    'SO2_MASS (lbs)',
    'NOX_MASS (lbs)',
    'CO2_MASS (tons)',
    col_heat,
    'FAC_ID',
    'UNIT_ID']


def parse_hourly(filename):
    data = read_csv(filename)
    data['time'] = conv.parse_date_time(
        data['OP_DATE'], data['OP_HOUR'].astype(str) + ':00:00')

    # 2003 data has different column names
    data = data.rename(columns={
        'GLOAD': col_energy,
        'SO2_MASS': 'SO2_MASS (lbs)',
        'NOX_MASS': 'NOX_MASS (lbs)',
        'CO2_MASS': 'CO2_MASS (tons)',        
        'HEAT_INPUT': col_heat,
        })
    
    # data in 2000 did not have IDs
    if 'FAC_ID' not in data.columns:
        data['FAC_ID'] = None        
    if 'UNIT_ID' not in data.columns:
        data['UNIT_ID'] = None
    return data[important_columns]

def nice_name_hourly(df):
    out_cols = ['unit_id', 'time', 'op_time', 'power', 'heat']
    out = df.rename(columns={
        'UNIT_ID': 'unit_id',
        'OP_TIME': 'op_time',
        'GLOAD (MW)': 'power',
        'HEAT_INPUT (mmBtu)': 'heat'
    })[out_cols]

    if len(out.unit_id.unique()) == 1:
        out = out.drop('unit_id', axis=1)

    return out.set_index('time')
    
def get_hourly_data(plant_id, uids, db):
    return nice_name_hourly(db.query(
        'select * from hourly where "ORISPL_CODE"={} and "UNIT_ID" in {} order by time'.format(int(plant_id), sql_in(uids))
        )).drop_duplicates()
                           

def get_hourly_ids():
    return read_csv(config.dir_parsed+'hourly-ids.csv', index_col=0)
