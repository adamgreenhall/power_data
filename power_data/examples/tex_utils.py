def tex_representation(coefs, digits=5):
    '''Output polynomial to tex-style string.'''
    texstr = ''
    exp = 0
    for n in reversed(coefs):
        if round(n, digits) == 0 and exp != 0:
            continue
        addChar = '+' if n > 0 else ''
        if exp > 1:
            texstr += '{pm}{n:0.{d}f}P^{exp}'.format(
                pm=addChar, n=n, exp=exp, d=digits)
        elif exp == 1:
            texstr += '{pm}{n:0.{d}f}P'.format(pm=addChar, n=n, d=digits)
        elif exp == 0:
            texstr += '{pm}{n:0.{d}f}'.format(pm=addChar, n=n, d=digits)
        exp += 1
    if texstr[0] == '+':
        texstr = texstr[1:]
    return texstr
