import urllib
import zipfile
import os
from glob import glob
import itertools
from collections import Counter

import yaml
import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

try:
    from ipdb import set_trace
except:
    from pdb import set_trace


def download(url, local_dir='.'):
    is_zip = url.endswith('.zip')
    if is_zip:
        localfile = 'tmp.zip'
    else:
        localfile = local_dir + url.split('/')[-1]

    urllib.urlretrieve(url, localfile)
    # result = requests.get(web_address)
    # with open(tmp, 'w+') as f: f.write(result.content)

    if is_zip:
        zipfile.ZipFile(localfile).extractall(local_dir)
        os.remove(localfile)


def ts_from_csv(filename, index_col=0, squeeze=True,
    timezone=None, is_df=False, **kwargs):
    kwargs['header'] = 0 if is_df else None

    df = pd.read_csv(filename, index_col=index_col, squeeze=squeeze, **kwargs)
    df.index = pd.DatetimeIndex(df.index)
    if timezone is not None:
        # pandas seems to convert any stamps to UTC in the DatetimeIndex call
        df.index = df.index.tz_localize('UTC').tz_convert(timezone)
    return df


def ilen(it):
    # the length of an iterable
    return sum(1 for _ in it)


def runs(list, on_value=True, more_than_count=0):
    # get the counts of run length encoding for values that match on_value
    # http://wordaligned.org/articles/runlength-encoding-in-python
    # if more_than_count is set, limit the results to runs that happen
    #  more frequently

    run_lengths = [ilen(
        grp) for value, grp in itertools.groupby(list) if value == on_value]
    if more_than_count:
        run_lengths = [run_len for run_len, count in Counter(
            run_lengths).items() if count > more_than_count]
    return run_lengths


def filter_within(df, col, a, b):
    return df[(a <= df[col]) & (df[col] < b)]


def dump_yaml_file(x, filename, mode='w+'):
    with open(filename, mode) as f:
        yaml.dump(x, f, default_flow_style=False)


def load_yaml_file(filename):
    with open(filename) as f:
        out = yaml.load(f)
    return out


def tex_representation(coefs, digits=5):
    '''Output polynomial to tex-style string.'''
    texstr = ''
    exp = 0
    for n in reversed(coefs):
        if round(n, digits) == 0 and exp != 0:
            continue
        addChar = '+' if n > 0 else ''
        if exp > 1:
            texstr += '{pm}{n:0.{d}f}P^{exp}'.format(
                pm=addChar, n=n, exp=exp, d=digits)
        elif exp == 1:
            texstr += '{pm}{n:0.{d}f}P'.format(pm=addChar, n=n, d=digits)
        elif exp == 0:
            texstr += '{pm}{n:0.{d}f}'.format(pm=addChar, n=n, d=digits)
        exp += 1
    if texstr[0] == '+':
        texstr = texstr[1:]
    return texstr


def get_limits(x, Nbins=20, freq_limit=0.025):
    '''
    Get limits based on binning. The min/max are found from bins which
    have a frequency greater than freq_limit.
    '''

    x = x[(x > 0.01) | (x < 0.01)]

    if (len(x) < 100) or (len(x.unique()) == 1):
        # we don't have enough data or there is only one value
        return x.min(), x.max()
        
    bins = pd.cut(x, Nbins)
    bin_groups = x.groupby(bins)
    counts = bin_groups.count()
    valid_bins = counts[counts >= freq_limit * x.count()].index

    minima = bin_groups.min().ix[valid_bins].min()
    maxima = bin_groups.max().ix[valid_bins].max()
    return minima, maxima


def fit_polynomial(xy, deg=3, round_digits=20):
    # polyfit expects x,y input - so split out xy
    coefs, resid_error, _, _, _ = np.polyfit(*zip(*xy), deg=deg, full=True)
    resid = resid_error[0]
    return coefs, resid


def read_dir_to_panel(dirnm, **kwds):
    return pd.Panel({
        os.path.split(fnm)[1].strip('.csv'): pd.read_csv(fnm, **kwds)
        for fnm in glob(os.path.join(dirnm, '*.csv'))
        })

# extend interpolate to the DataFrame
def interpolate(df, **kwds): 
    return pd.DataFrame({
        nm: series.interpolate(**kwds) for nm, series in df.iterkv()
        })[df.columns]
pd.DataFrame.interpolate = interpolate

# order a DataFrame by the last index values (useful for plotting and legends)
def end_ordered(df): return df[df.iloc[-1].order(ascending=False).index]
pd.DataFrame.end_ordered = end_ordered

# create a plotting method for Panel
def plot_panel(pn, sharex=False, sharey=False, figsize=(12,5), 
    dfmod=None, orient='horizontal', **kwds): 
    if orient == 'horizontal':
        plt_seq = (1, len(pn.items))
    elif orient == 'vertical':
        plt_seq = (len(pn.items), 1)
        
    fig, axes = plt.subplots(*plt_seq, sharex=sharex, sharey=sharey, 
        tight_layout=True, figsize=figsize)
    if dfmod is None:
        dfmod = lambda df: df
    for i, nm in enumerate(pn.items):
        dfmod(pn[nm]).plot(ax=axes[i], **kwds)
        axes[i].set_title(nm)
        if orient == 'vertical' and nm != pn.items[-1]: axes[i].set_xlabel('')
    return fig, axes
pd.Panel.plot = plot_panel