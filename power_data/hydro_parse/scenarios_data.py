import os
import pandas as pd
import utils
from utils import ts_from_csv, set_trace, debug_exceptions
debug_exceptions()


basedir = '../data-parsed/BPA/'
bpa_datadir = os.path.expanduser(
    '~/research/bpa-experiment/data/')


filename_project_data = bpa_datadir + 'BPA-project-data.xls'
filename_network_data = bpa_datadir + 'reservoir-network.csv'
filename_prices = '../data-created/BPA/midc-prices-2010.csv'
filename_export_max = basedir + 'BPA-intertie-capacity-2010.csv'
filename_wind_data = basedir + 'BPA-2010-hourly.csv'

tz = 'US/Pacific'

inflow_sheet_nms = {
    'scenario-one': 'Scenario_I_Flows',
    'scenario-two': 'Scenario_II_Flows'
}
demand_sheet_nms = {
    'scenario-one': 'Scenario_I_Loads',
    'scenario-two': 'Scenario_II_Loads'
}

def create_export_data(times):
    # use modeled MidC prices and ACDC intertie limits
    export_data = pd.DataFrame({
        'price_export': ts_from_csv(filename_prices),
        'export_max':   ts_from_csv(filename_export_max)}).ix[times]
    export_data.index.name = 'time'
    return export_data

# create initial conditions
def create_initial(gen_data, historic_data, volCurves, times):
    tm = times[0] - pd.DateOffset(hours=1)
    columns = ['power', 'elevation', 'outflow', 'spill', 'volume']
    initial = historic_data.transpose(1,0,2)[tm]\
        .rename(columns={'elevation_forebay': 'elevation'})[columns]\
        .ix[gen_data.index]
    initial['volume'] = pd.Series({
        nm: utils.get_volume_from_elevation(
            volCurves[nm],
            initial.ix[nm, 'elevation'])
        for nm in initial.index
        })
    return initial


# inflow scenarios
def parse_inflows(xls, sheetnm):
    df = xls.parse(sheetnm, skiprows=5)
    df = df.set_index(df.columns[0])
    df.index.name = 'time'
    df = df.rename(columns={c:
        'inflow_' + (
            c.split('-')[1] if '-' in c else c.split(' ')[0])
        for c in df.columns})
    # BPA times are hour ending - convert to hour begin
    return df.shift(-1, freq='H').tz_localize('US/Pacific')

# demand scenarios
def parse_demand(xls, sheetnm):
    df = xls.parse(sheetnm, skiprows=5)
    # days are in columns, times are rows.
    times = pd.date_range(
        df.columns[1],
        df.columns[-1]+pd.DateOffset(hour=23),
        freq='H')
    # convert to a series
    demand = pd.Series(df[df.columns[1:]].values.flatten("F"),
        index=times, name='power')
    demand.index.name = 'time'
    return pd.DataFrame(demand).tz_localize('US/Pacific')


def create_elevation_targets(projects, flows, times):
    nms = projects.index
    projects['elevation_target_max_schedule'] = pd.Series(
        {p: 'elevation_target_max_{}'.format(p) for p in nms})
    projects['elevation_target_min_schedule'] = pd.Series(
        {p: 'elevation_target_min_{}'.format(p) for p in nms}) 

    mins = flows.minor_xs('elevation_forebay').resample(
        'W', how='min', convention='end').asfreq('H').interpolate()
    maxes = flows.minor_xs('elevation_forebay').resample(
        'W', how='max', convention='end').asfreq('H').interpolate()

    # not all of the historical data meets the limits - fixup
    for proj in maxes.columns:
        maxes.ix[maxes[proj] > projects.ix[proj, 'elevation_max'], proj] = \
            projects.ix[proj, 'elevation_max']
    return maxes\
        .rename(columns={nm: 'elevation_target_max_%s'%nm for nm in nms})\
        .join(
        mins.rename(columns={nm: 'elevation_target_min_%s'%nm for nm in nms}))\
        .ix[times]

xls = pd.ExcelFile(filename_project_data)
gen_data = pd.read_csv(basedir + 'hydro.csv', index_col=0)
historic_data = pd.HDFStore(
    basedir + 'historical-flows/BPA-2010-flows.hd5', mode='r').flows
volCurves = utils.read_dir_to_panel(basedir + 'original_curves/volume_to_forebay_elevation')
wind_data = ts_from_csv(filename_wind_data)

for scenario in inflow_sheet_nms.keys():
    os.system('mkdir -p {}'.format(basedir + scenario))
    inflows = parse_inflows(xls, inflow_sheet_nms[scenario])
    demand = parse_demand(xls, demand_sheet_nms[scenario])
    times = demand.index
    # use historic demand instead of scenario demand
    demand = wind_data['load'].ix[times]
    
    exports = create_export_data(times)
    initial = create_initial(gen_data, historic_data, volCurves, times)
    targets = create_elevation_targets(gen_data, historic_data, times)
    schedule = inflows.reindex(inflows.index.union(times))\
        .asfreq('H').interpolate()\
        .join(targets)
    history = historic_data.to_frame()\
        .rename({'elevation_forebay': 'elevation'})\
        .reset_index()\
        .set_index('time').tz_localize('UTC').tz_convert(tz)\
        .ix[pd.date_range(
            end=times[0]-pd.DateOffset(hours=1), periods=48, freq='H')]\

    scenario_dir = basedir + scenario + '/'
    schedule.ix[times].to_csv(scenario_dir + 'hydro_schedule.csv')
    demand.to_csv(scenario_dir + 'load_schedule.csv', header=True)
    exports.to_csv(scenario_dir + 'exports.csv')
    initial.to_csv(scenario_dir + 'hydro_initial.csv')
    history.to_csv(scenario_dir + 'hydro_history.csv')
    
    pd.Series(wind_data['wind'].ix[times], name='power').to_csv(
        scenario_dir + 'wind_observed.csv', header=True)
    pd.Series(wind_data['wind forecast'].ix[times], name='power').to_csv(
        scenario_dir + 'wind_forecast.csv', header=True)
