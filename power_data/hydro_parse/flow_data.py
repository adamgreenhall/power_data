"""
parser for data from the USACE dataquery site
currently set up to read hourly and daily flow data for the
Columbia River projects.
"""
import os
import urllib
import urlparse
import numpy as np
import pandas as pd
from ipdb import set_trace
import utils

def read_csv_ts(filename, tz='US/Pacific', **kwds):
    return pd.read_csv(filename, index_col=0, parse_dates=True).tz_localize('UTC').tz_convert(tz)



# for example
# url = "http://www.nwd-wc.usace.army.mil/perl/dataquery.pl?k=id%3ABON%2Brecord%3A%2F%2FBON%2FQG%2F%2FIR-MONTH%2FHRXZZAZD%2F%2Bpk%3Aid.bon%2Brecord.%2F%2Fbon%2Fqg%2F%2Fir-month%2Fhrxzzazd%2F&sd=01&sm=JAN&sy=2012&ed=31&em=DEC&ey=2012&of=Text+Comma-Delimited&et=Screen&dc=One+Column&snpt=Daily&snph=00&snpw=15&f1m=MAY&f1d=04&f2sm=MAY&f2sd=04&f2em=MAY&f2ed=04&f3c=Less+Than+Or+Equal+To&f3t="
# params = urlparse.parse_qs(url.split('?')[1])


start_date = pd.Timestamp('2011-12-30')
end_date = pd.Timestamp('2012-12-31')
download_mode = False

fnm_storage = '../data-parsed/BPA/historical-flows/BPA-2012-flows.hd5'

# build out the URL
prefix='http://www.nwd-wc.usace.army.mil/perl/dataquery.pl'
fields = dict(
    outflow="QG",
    spill="QS",
    inflow="QI",
    elevation_forebay="HF",
    elevation_tailwater="HT",
    power="VE",  # actually this is hourly energy
    volume="LS", # in KAF
)
field_freq = pd.Series('H', fields.keys())
field_freq[['inflow', 'elevation_forebay', 'elevation_tailwater']] = 'D'
field_freq.ix['volume'] = '6H'

usace_freq_names = {'H': 'HRX', 'D': 'DRX', '6H': 'QFZ'}

default_params = {
    'k': 'id:BON+record://BON/QG//IR-MONTH/HRXZZAZD/',

    'dc': 'One Column',
    'of': 'Text Comma-Delimited',
    'et': 'File',

    'ed': 31,
    'em': 12,
    'ey': 2012,

    'sd': 1,
    'sm': 1,
    'sy': 2012
    }

def build_url(start_date, end_date, project="BON", name="outflow"):
    params = default_params.copy()
    params.update({
        'k': 'id:{p}+record://{p}/{f}//IR-MONTH/{freq}ZZAZD/'.format(
            p=project,
            f=fields[name],
            freq=usace_freq_names[field_freq[name]]),
        'sy': start_date.year,
        'sm': start_date.month,
        'sd': start_date.day,
        'ey': end_date.year,
        'em': end_date.month,
        'ed': end_date.day,
        })
    return prefix+'?'+urllib.urlencode(params)

def parse_dates_vals(ts):
    ts.columns = [c.strip() for c in ts.columns]
    ts = ts.ix[(pd.isnull(ts.Date) | pd.isnull(ts.Time)) == 0]
    ts = ts.set_index((ts.Date.str[:2] + '-' + ts.Date.str[2:5] + '-' + ts.Date.str[5:9] + ' ' + ts.Time + ':00').apply(pd.Timestamp))
    return ts['Data']

def get_data(start_date, end_date, project="BON", dataset="outflow"):
    url = build_url(start_date, end_date, project, dataset)

    times_hourly = pd.Index(pd.date_range(
        start_date,
        #end_date if is_daily else
        end_date + pd.DateOffset(hour=23),
        # freq='D' if is_daily else 'H',
        freq='H',
        tz='US/Pacific'), name='time')
    if not field_freq[dataset] == 'H':
        times = pd.Index(pd.date_range(start_date, end_date, freq=field_freq[dataset], tz='US/Pacific'))
    else:
        times =  times_hourly
    try:
        ts = pd.read_csv(url, skiprows=9)
        print '  {}'.format(dataset)
    except:
        if dataset == 'volume':
            print('  error downloading {} {}'.format(project, dataset))
            return pd.Series(None, times_hourly)
        else:
            set_trace()

    if times.freq == '6H':
        # considered irregular - must parse the times
        ts = parse_dates_vals(ts)
    else:
        ts = pd.Series(
            ts[' Data'].interpolate().values[:len(times)],
            index=times, name=dataset)

    if ts.index.freq != 'H':
        ts = ts.reindex(times_hourly).interpolate().fillna(method='bfill')

    return ts

projects = pd.read_csv('../data-parsed/BPA/hydro.csv', index_col=0)
upstream_reservoirs = {
    project: list(projects[projects.downstream_reservoir == project].index)
    for project in projects.index}

flows = {}
if download_mode:
    for project in projects.index:
        print project
        flows[project] = pd.DataFrame({
            field: get_data(start_date, end_date, project, field) for field in fields.keys()
            })
    flows = pd.Panel(flows)
else:
    flows = pd.HDFStore(fnm_storage).flows


# compute incremental inflows
def get_incremental_inflows(name):
    def get_net_outflow(nm, delay=1):
        return flows[nm][['outflow', 'spill']].sum(axis=1).shift(delay)
    
    if upstream_reservoirs[name]:
        delays = {
            unm: round(projects.delay_downstream[unm]) 
            for unm in upstream_reservoirs[name]}
        net_upstream_outflows = pd.DataFrame({
            unm: get_net_outflow(unm, delays[unm]) for unm in upstream_reservoirs[name]
            }).sum(axis=1)
        inc_inflow = flows[name].inflow - net_upstream_outflows
    else:
        inc_inflow = flows[name].inflow.copy()

    # lowpass filter to smooth the incremental inflows
    return inc_inflow.resample('1D', how='mean').asfreq('H').interpolate()

if 'incremental_inflow' not in flows.minor_axis:
    flows = pd.Panel({nm: df.join(
        pd.Series(np.nan, index=df.index, name='incremental_inflow')
        ) for nm, df in flows.iteritems()})
    
for project in projects.index:
    flows[project]['incremental_inflow'] = get_incremental_inflows(project)
    flows[project][flows[project].index.year == 2012].to_csv(
        '../data-parsed/BPA/historical-flows/{}.csv'.format(project))

store = pd.HDFStore(fnm_storage)
store['flows'] = flows

inc_inflows = flows.minor_xs('incremental_inflow')\
    .rename(columns={nm: 'inflow_' + nm for nm in flows.items})
inc_inflows[inc_inflows.index.year == 2012].to_csv(
    '../data-parsed/BPA/historical-flows/BPA-incremental-inflows-2012.csv')
    
outflow_limits = pd.DataFrame({
'outflow_min': flows.minor_xs('outflow').min(),
'outflow_max': flows.minor_xs('outflow').max()})

outflow_limits.to_csv('../data-parsed/BPA/historical-flows/outflow-limits.csv')



# elevation targets
storage_projects = projects.index #['GCL', 'CHJ', 'MCN', 'JDA', 'TDA']

el = flows.minor_xs('elevation_forebay')
mins = el.resample('W', how='min', convention='end')
maxes = el.resample('W', how='max', convention='end')
# not all of the historical data meets the limits - fixup
for proj in maxes.columns:
    maxes.ix[maxes[proj] > projects.ix[proj, 'elevation_max'], proj] = projects.ix[proj, 'elevation_max']
    
elevation_targets = pd.Panel({
    'elevation_target_min': mins.ix[mins.index.year == 2012, storage_projects],
    'elevation_target_max': maxes.ix[mins.index.year == 2012, storage_projects]
    }).transpose(2, 1, 0)


dirnm = '../data-parsed/BPA/elevation-targets/'
os.system('mkdir -p {}'.format(dirnm))
for nm, df in elevation_targets.iteritems():
    df.to_csv('{}{}.csv'.format(dirnm, nm))
    
df_sched = pd.DataFrame(index=elevation_targets.major_axis)
for nm, df in elevation_targets.iteritems():
    df_sched = df_sched.join(df.rename(
        columns={c: '{}_{}'.format(c, nm) for c in df.columns}))

df_sched = pd.DataFrame({nm: series.asfreq('H').interpolate()
    for nm, series in df_sched.iteritems()})

hydro_schedule = read_csv_ts(
    '../data-parsed/BPA/historical-flows/BPA-incremental-inflows-2012.csv')

hydro_schedule.join(df_sched).fillna(method='ffill').to_csv(
    '../data-parsed/BPA/historical-flows/BPA-incremental-inflows-targets-2012.csv')
