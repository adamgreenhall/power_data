import requests
import numpy as np
import pandas as pd
from bs4 import  BeautifulSoup
from ipdb import set_trace
import re

patSplit = re.compile('[:=]|\.+')
pat_antiFloat = re.compile(',|[^\d|.]')

fields = [
    'Stream',
    'Location',
    'Type of Project',
    # Lake Elevation
    'Maximum forebay',
    'Full forebay',        
    'Minimum forebay',    
    # 
    'Maximum rate of change per hour',
    'Number of units',
    'Nameplate capacity',
    'Overload capacity',
    'Hydraulic capacity',
]

alt_fields = [
    'Maximum pool',
    'Full pool', 
    'Minimum pool',
    'Number of main units'
    ]
units = ['ft', 'cfs', 'MW']

equivNames = dict([
    ('{} pool'.format(level), '{} forebay'.format(level))
    for level in ['Maximum', 'Full', 'Minimum']])
equivNames['Number of main units'] = 'Number of units'

def dam_info_url(dam_id):
    return 'http://www.nwd-wc.usace.army.mil/report/{}.htm'.format(dam_id.lower())

dams = pd.read_csv('../data-parsed/BPA/columbia_dams.csv')

def get_dam_info(did):
    def get_field(fnm, lines):
        out = map(
            lambda ln: re.split(patSplit, ln.strip(), maxsplit=1)[1].strip(), 
            filter(lambda ln: ln.lower().strip().startswith(fnm.lower()), lines))
        if len(out) == 1:
            try:
                outstr = out[0]
                for unit in units + [',']:
                    outstr = outstr.replace(unit, '').strip()
                return float(outstr)
            except:
                return out[0]
        elif len(out) == 0:
            return np.nan
        else:
            return sum(map(lambda x: float(
                re.sub(pat_antiFloat, '', x)), out))
    req = requests.get(dam_info_url(did))
    results = {}
    if req.status_code == 200:
        lines = BeautifulSoup(req.text).get_text().splitlines()
        for f in fields + alt_fields:
            results[f] = get_field(f, lines)
    else:
        print 'did not find {}'.format(did)
    return results


dam_info = pd.DataFrame(
    columns=pd.Series(fields).values,
    index=dams.dam_ID.dropna().values)
for i in dam_info.index:
    dam_info.ix[i] = pd.Series(get_dam_info(i)).dropna().rename(equivNames)
print dam_info

dams.join(dam_info, on='dam_ID').to_csv(
    '../data-parsed/BPA/columbia_dams_capacities.csv', index=False)

set_trace()
