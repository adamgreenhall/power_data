import pandas as pd
import numpy as np
from utils import ts_from_csv, set_trace, debug_exceptions
debug_exceptions()
tz = 'US/Pacific'

# off peak and peak pricing data has to be downloaded from [ICE reports](https://www.theice.com/marketdata/reports/ReportCenter.shtml#report/54)
# Peak pricing only is available from the [EIA directly](http://www.eia.gov/electricity/wholesale/archive/2012/midc_12.xls)

# peak and off peak defined from [Dow Jones](http://www.djindexes.com/mdsidx/downloads/brochure_info/Dow_Jones_Mid-Columbia_Electricity_Price_Indexes_Overview.pdf) to be:
# * on peak:  Hours ending 0700 - 2200 (6 a.m. - 10 p.m.)
# * off peak: Hours ending 2300 - 0600 (10 p.m. - 6 a.m.)


year = 2010

def peak_hrs(day):
    dt = pd.Timestamp(day).tz_localize(tz)
    return pd.date_range(
        dt + pd.DateOffset(hour=7),
        dt + pd.DateOffset(hour=21),
        freq='H')
def offpeak_hrs(day):
    '''get both periods of off peak pricing'''
    dt = pd.Timestamp(day).tz_localize(tz)
    return [
        pd.date_range(
            dt + pd.DateOffset(hour=0),
            dt + pd.DateOffset(hour=6), freq='H'),
        pd.date_range(
            dt + pd.DateOffset(hour=22),
            dt + pd.DateOffset(hour=23), freq='H')]

def wholeday_hrs(day):
    dt = pd.Timestamp(day).tz_localize(tz)
    return pd.date_range(
        dt + pd.DateOffset(hour=0),
        dt + pd.DateOffset(hour=23), freq='H')

def rescale(series, newmin=0, newmax=1):
    return newmin + (newmax - newmin) * \
        (series - series.min()) / (series.max() - series.min())
pd.Series.rescale = rescale

def get_prices(filename, year):
    colnames = {
        'BEGIN DATE':'date',
        'END DATE':'date_end',
        'HIGH':'price_high',
        'LOW':'price_low',
        'AVG':'price_avg',
        'VOL (MWH)':'energy_traded',
    }
    prices = pd.read_csv(filename,
        parse_dates=['BEGIN DATE', 'END DATE'])\
        .rename(columns=colnames)[colnames.values()]

    irreg_periods = prices[prices.date != prices.date_end]

    prices = prices[prices.date == prices.date_end]\
        .drop(['date_end'], axis=1)

    for i, row in irreg_periods.iterrows():
        df = pd.DataFrame(
            row.drop(['date', 'date_end']).to_dict(),
            index=pd.date_range(row['date'], row['date_end'], freq='D'))
        df.index.name = 'date'
        df['energy_traded'] = np.nan
        prices = prices.append(df.reset_index())

    prices = prices.sort('date').set_index('date')
    return prices.ix[prices.index.year == year]

prices_peak = get_prices(
    '../data-raw/BPA/midc-peak-{}.csv'.format(year), year)
prices_off_peak = get_prices(
    '../data-raw/BPA/midc-off-peak-{}.csv'.format(year), year)

# load
demand = pd.DataFrame(dict(
    BPA= ts_from_csv(
        '../data-parsed/BPA/BPA-%s-hourly.csv' % year)['load'],
    CAISO= ts_from_csv(
        '../data-parsed/CAISO/CAISO-demand-%s.csv' % year)
    )).sum(axis=1)



prices_synth = pd.Series(np.nan, index=demand.index)
for day, dem in demand.groupby(demand.index.map(lambda t: t.date())):
    day = pd.Timestamp(day)
    try:
        pr_pk = prices_peak.ix[day]
        hrs_pk = peak_hrs(day)
        prices_synth.ix[hrs_pk] = dem.ix[hrs_pk].rescale(
            pr_pk['price_low'], pr_pk['price_high'])
        is_off_peak_day = False
    except KeyError:
        is_off_peak_day = True

    pr_off = prices_off_peak.ix[day]

    if is_off_peak_day:
        hrs_off = [wholeday_hrs(day)]
    else:
        hrs_off = offpeak_hrs(day)

    for hrs in hrs_off:
        prices_synth.ix[hrs] = dem.ix[hrs].rescale(
            pr_off['price_low'], pr_off['price_high'])

prices_synth = prices_synth.interpolate()
prices_synth.index.name = 'time'

prices_synth.name = 'price'
prices_synth.to_csv(
    '../data-created/BPA/midc-prices-%s.csv' % year, header=True)
