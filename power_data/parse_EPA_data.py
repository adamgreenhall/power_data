"""
This data originally comes from public datasets at the EPA and DOE-EIA:
* EPA Clean Air Markets hourly emission data:   ftp://ftp.epa.gov/dmdnload/emissions/hourly/monthly
* EPA eGrid annual plant,generator,boiler data: http://www.epa.gov/cleanenergy/energy-resources/egrid/
    
The goal is to take the data and for each generator get:
* unique name
* heat rate curve
* power min/max
* ramp rate min/max
* min up/down time
* prime mover type
* fuel type
"""

