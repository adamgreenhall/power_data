import os
from glob import glob
import numpy
import pandas as pd
import config
from database import DatabaseConnection
from database import _execute as db_execute
from parse_hourly import parse_hourly
from ipdb import set_trace

def add_files_to_database(files=None):
    if files is None:
        searchstr = '*.csv'    
        files = sorted(glob(config.dir_raw_hourly + searchstr))
        # ignore the texas files (already in the database)
        # files = filter(lambda f: not 'tx' in f, files)
        
    print('{} files'.format(len(files)))
    
    for i,f in enumerate(files):
        print(i)
        data = parse_hourly(f)
        data.to_csv('tmp.csv')
        try: os.system('psql -d egrid -f database_append.sql')
        except:
            print('error on ', f)
            raise

    os.remove('tmp.csv')

def clean_missing_data():
    # this takes forever - almost certainly would be better off doing this before the initial insert
    db = DatabaseConnection('egrid')
    db.connect()
    # units = db.query('select distinct ("ORISPL_CODE","UNIT_ID"), "ORISPL_CODE","UNIT_ID","UNITID" from hourly')
    store = pd.HDFStore('tmp.hd5', mode='r')
    units = store['units']
    
    if pd.isnull(units.UNIT_ID).any():
        # correct for null UNIT_IDs by matching plant_id and unit_name
        for (plant_id, unit_name), unit_ids in units.groupby(["ORISPL_CODE", "UNITID"]).UNIT_ID:
            if not pd.isnull(unit_ids).any():
                continue
            unit_id = unit_ids.dropna()
            if len(unit_id) == 1:
                unit_id = int(unit_id.values[0])
            else:
                set_trace()
            
            # update the UNIT_ID values in the database
            sql = 'UPDATE hourly SET "UNIT_ID"={unit_id} \
                where "ORISPL_CODE"={plant_id} \
                and "UNITID"=\'{unit_name}\' \
                and "UNIT_ID" is null;'.format(
                    unit_id=unit_id,
                    unit_name=unit_name,
                    plant_id=plant_id)
            print((plant_id, unit_id, unit_name))
            cur = db_execute(sql, db.con)
            print(cur.statusmessage)
        else:
            # dont forget to commit all those changes
            db.con.commit()
            # now that we have mucked around with values in the database 
            # re-index the table so that the queries perform faster
            db_execute('reindex table hourly', db.con, commit=True)
    

def get_unit_metadata():
    # create a dataframe of units in the hourly database
    db = DatabaseConnection('egrid')
    db.connect()
    print('getting unique units')
    units = db.query('select distinct ("ORISPL_CODE","UNIT_ID"), "ORISPL_CODE","UNIT_ID","UNITID" from hourly')
    print("fetched {} unique units".format(len(units)))
    
    # fetch the max. power for each unit
    units['max_power'] = numpy.nan
    print len(units[pd.notnull(units.UNIT_ID)])
    for i, unit in units[pd.notnull(units.UNIT_ID)].iterrows():
        print i
        units.ix[i, 'max_power'] = db.max_query('GLOAD (MW)', 'hourly', 'where "UNIT_ID"={id}'.format(id=int(unit['UNIT_ID'])))
    
    cols = ['ORISPL_CODE', 'UNIT_ID', 'UNITID', 'max_power']
    
    units[cols].to_csv(config.dir_parsed+'hourly-ids.csv')

    db.con.close()

if __name__ == '__main__':
    # add_files_to_database()
    get_unit_metadata()
