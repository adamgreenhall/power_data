# minpower's setup script

#build using:
#python setup.py sdist

#udpate pypi using:
#python setup.py register
#python setup.py sdist upload
#git tag -a v2.0 -m 'version 2.0'; git push --tags
#info: http://packages.python.org/distribute/setuptools.html#basic-use


from setuptools import setup, find_packages
setup(
    name = "power_data",
    version = "0.1",
    # download_url = "https://github.com/adamgreenhall/power_data/zipball/v0.1",

    # entry_points="""
    # [console_scripts]
    # minpower = minpower.solve:main
    # """,

    install_requires=[
        'pandas>=0.9',
        'requests',
        'xlrd',
        'openpyxl',
        'keyring',
        'ordereddict>=1.1',
        'PyYAML>=3.10',
        'matplotlib>=1.0.1',
        'psycopg2',
    ],

    # description = "power systems optimization made beautiful",
    author = "Adam Greenhall",
    author_email = "adam.greenhall@gmail.com",
    # url = "http://minpowertoolkit.com/",

    packages = find_packages(),
    keywords = ["power systems","data"],
    
    classifiers = [    
        "Development Status :: 4 - Beta",
        "Environment :: Other Environment",
        "Intended Audience :: Science/Research",      
        "Intended Audience :: Education",
        "License :: OSI Approved :: MIT License",
        "Natural Language :: English",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Topic :: Scientific/Engineering",
        "Topic :: Scientific/Engineering :: Mathematics",
        ],
    long_description = """"""
)
